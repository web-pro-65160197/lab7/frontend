import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import memberService from '@/services/member'
import type { Member } from '@/types/Member'

export const useMemberStore = defineStore('member', () => {
  const loadingStore = useLoadingStore()
  const members = ref<Member[]>([])

  async function getMember(id: number) {
    loadingStore.doload()
    const res = await memberService.getMember(id)
    members.value = res.data
    loadingStore.finish()
  }

  async function getMembers() {
    loadingStore.doload()
    const res = await memberService.getMembers()
    members.value = res.data
    loadingStore.finish()
  }
  
  async function saveMember(member: Member) {
    loadingStore.doload()
    if(member.id < 0) { // Add new
      const res = await memberService.addMember(member)
    } else { // Update
      const res = await memberService.updateMember(member)
    }
    await getMembers()
    loadingStore.finish()
  }
  
  async function deleteMember(member: Member) {
    loadingStore.doload()
    const res = await memberService.delMember(member)
    await getMembers()
    loadingStore.finish()
  }

  const getMemberByTel = (tel: string): Member | null => {
    for (const member of members.value) {
      if (member.tel === tel) return member
    }
    return null
  }
  return { members, getMember, getMembers, saveMember, deleteMember, getMemberByTel }
})
